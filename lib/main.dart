import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'app',
      theme: ThemeData.dark(),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime selectDate = DateTime.now();

  void DatePicker() {
    showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2020), lastDate: DateTime.now()).then(
      (value) {
        if (value == null) {
          return;
        } else {
          print(selectDate);
          setState(()=>selectDate = value);
        }
      },
    );
  }

  String format(DateTime dateTime){
    return 'Day:${dateTime.day} Month:${dateTime.month} Year:${dateTime.year}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${format(selectDate)}'),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(150, 200, 150, 200),
            alignment: Alignment.center,
            child: ElevatedButton(
              onPressed: DatePicker,
              child: Text(
                'Selecting a date',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DateFormater {
  static String formatDate(DateTime datetime) {
    DateFormat dateFormat = DateFormat.yMd();
    return dateFormat.format(datetime);
  }
}
